package org.isima.scoreliste.Controller;

import org.isima.scoreliste.Entity.Produit;
import org.isima.scoreliste.services.OpenFoodService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author mariviere4
 *Classe contrôleur de l'accès à l'API OpenFood
 */


public class OpenFoodController {
	//Service utilisé par le contrôleur
    @Autowired
    private OpenFoodService openFoodService;

    // Changement du service essentiellement utilisé dans les tests
    public void setOpenFoodService(OpenFoodService os)
    {
    	openFoodService = os;
    }
    
    // Appel du service pour récupérer les informations du produit ayant le code passé en paramètre
    public Produit getProduit(String code){
        Produit p = openFoodService.getProduit(code);
        p.codeBarre = Long.valueOf(code);
        return p;
    }
}
