package org.isima.scoreliste.Controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.isima.scoreliste.Entity.Liste;
import org.isima.scoreliste.Entity.Produit;
import org.isima.scoreliste.Entity.Rule;
import org.isima.scoreliste.Entity.Score;
import org.isima.scoreliste.services.ListeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ListeController {
	
	@Autowired
	private ListeService listeService;

	@Autowired
	private ProduitController produitController;

	
	  // Getters et setters 
    public ListeService getListeService() {
		return listeService;
	}

	public ProduitController getProduitController() {
		return produitController;
	}
	
	public void setListeService(ListeService ls) {
		listeService = ls;
	}
	
	public void setProduitController(ProduitController pc) {
		produitController = pc;
	}
	
	public Liste listecourses = new Liste();
	public List<String> listeCodeBarre = new ArrayList<String>();
	
	
	//Route pour obtenir la liste des produits avec le score nutritionnel calculé
	@GetMapping(value = "/produitsScore")
	public ResponseEntity<Liste> getProduitsScore()
	{
		//Récupération de la liste des codes barres de l'api de gestion de liste
		listeCodeBarre = listeService.getCodeBarres();
		
		//Parcours de la liste de codes barres
		for(String s : listeCodeBarre)
		{
			//Ajout du produit calculé
			listecourses.ajouterProduit(produitController.getOpenFoodController().getProduit(s));		
		}
		
		//Retour de la nouvelle liste
		return ResponseEntity.ok(listecourses);
	}
	


	// Route get produit pour avoir les qualités, les défauts et le score du produit avec le code passé en paramètre de la route
	@GetMapping(value= "/produit/{code}")
    public ResponseEntity<Produit> getProduit(@PathVariable String code){
		//Récupération du produit 
        Produit p = produitController.getOpenFoodController().getProduit(code);
        
        //Récupération de toutes les règles existantes dans le fichier JSON rules.json
        ArrayList<Rule> rules = produitController.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("rules.json").getFile(), Rule.class);
        
        //Récupération des différents types de scores dans le fichier JSON score.json
        ArrayList<Score> scores = produitController.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("score.json").getFile(), Score.class);
       
        //Création d'une liste des noms des nutriments entrants dans la composante négative
        ArrayList<String> negatifs = new ArrayList<>();
        negatifs.addAll(Arrays.asList("energy_100g", "saturated-fat_100g", "sugars_100g","salt_100g"));
        
        //Création d'une liste des noms des nutriments entrants dans la composante positive
        ArrayList<String> positifs = new ArrayList<>();
        positifs.addAll(Arrays.asList("fiber_100g", "proteins_100g"));
        
      //Création de la map des règles pour chaque nutriment
    	HashMap<String, ArrayList<Rule>> hashRules = produitController.getComponentsService().getRulesByNutriment(rules);
        //Calcul des deux composantes
        p = produitController.calculComponent(hashRules,p, rules, negatifs,"N");
        p = produitController.calculComponent(hashRules,p, rules, positifs,"P");
        
        //Calcul du score
        p = produitController.calculScore(p, scores);
        
        //Retour du produit
        return ResponseEntity.ok(p);
    }
	
}
