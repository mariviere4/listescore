package org.isima.scoreliste.Controller;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

/**
 * @author mariviere4
 * Classe contrôleur pour la lecture des fichiers JSON
 */


public class FileController
{
	/*Gets elements du fichier passé en paramètre
	 * file: nom du fichier à lire 
	 * classe : classe des objets à créer à partir du fichier passé en paramètre 
	 * */
    @SuppressWarnings("unchecked")
    public  <T> ArrayList<T> getElementsFromFile(String file, Class<T>  classe)
    {
    	 ArrayList<T> rulesObject = new ArrayList<>();
        //Parse du JSON
        JSONParser jsonParser = new JSONParser();
        try (FileReader reader = new FileReader(file))
        {

            //Lecture du fichier
            Object obj = (Object) jsonParser.parse(reader);

            JSONArray rules = (JSONArray) obj;            
           
            //Parcours du tableau d'objets JSON
            rules.forEach( r -> rulesObject.add(parseObject( (JSONObject) r , classe )));

        } catch (FileNotFoundException e) {
        	//Catch des problèmes d'ouverture de fichiers
            e.printStackTrace();
        } catch (IOException e) {
        	//Catch des problèmes de lecture
            e.printStackTrace();
        } catch (ParseException e) {
        	// Catch des problèmes de parse
            e.printStackTrace();
        }
        return rulesObject;
    }

    /*Parse un objzet JSOn en objet de la classe passée en paramètre
	 * o: objet JSON
	 * classe : classe de l'objet à créer 
	 * */
    private  <T> T parseObject(JSONObject o, Class<T> classe)
    {
    	
    	T r  = null;
        Gson gson = new Gson();
        r=gson.fromJson(o.toJSONString() , classe  );
        return r;
    }
}