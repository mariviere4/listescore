package org.isima.scoreliste.Controller;

import java.util.ArrayList;
import java.util.HashMap;

import org.isima.scoreliste.Entity.Nutriment;
import org.isima.scoreliste.Entity.Produit;
import org.isima.scoreliste.Entity.Rule;
import org.isima.scoreliste.Entity.Score;
import org.isima.scoreliste.services.ComponentsService;
import org.springframework.beans.factory.annotation.Autowired;

//Classe de contrôleur pour pouvoir calculer le score nutritionnel
public class ProduitController {
    
	//Service de component calculant les composantes 
    @Autowired
    private ComponentsService componentsService;

    //Contrôleur d'accès à l'API de OpenFood
    @Autowired
    private OpenFoodController openFoodController;

    //Contrôleur pour la lecture des fichiers JSON
    @Autowired
    private FileController fileController;
   
    // Getters et setters 
    public ComponentsService getComponentsService() {
		return componentsService;
	}

	public OpenFoodController getOpenFoodController() {
		return openFoodController;
	}

	public FileController getFileController() {
		return fileController;
	}

	public void setComponentsService(ComponentsService componentsService) {
		this.componentsService = componentsService;
	}

	public void setOpenFoodController(OpenFoodController openFoodController) {
		this.openFoodController = openFoodController;
	}

	public void setFileController(FileController fileController) {
		this.fileController = fileController;
	}

	/*Calcule le score du produit p en fonction des scores prédéfinis*/
    public Produit calculScore(Produit p, ArrayList<Score> scores) {
		double pointScore =0 ;
		//Calcul du score 
		pointScore = p.componentN  -p.componentP;
		
		//Comparaison avec les différents types de scores et leur bornes
		for(Score sc : scores)
		{
			if(pointScore > sc .lowerBound && pointScore < sc.upperBound )
			{
				p.score = sc;
			}
		}
		return p;
	}

    /*Calcule une composante
     * hasRules: map des différentes liste de règles pour chaque nutriment
     * p : produit dont on est en train de calculer la composante
     * rules: règles pour calculer les points des composantes
     * nutriments: liste des noms des nutriments de la composante à calculer
     * componen: composnate à calculer ( P ou N)
     * */
	public Produit  calculComponent(HashMap<String, ArrayList<Rule>> hashRules, Produit p, ArrayList<Rule> rules, ArrayList<String> nutriments , String component)
    {
		
		
    	int points = 0;
    	Nutriment n = null;
    	//Parcours de tous les éléments de la  composante
    	for(String s : nutriments)
    	{
    		//Récupération des points du nutriment
    		n =  componentsService.getPointsFromNutriment(hashRules, s , p.getValue(s), component);
    		
    		//Ajout des points à la composante
    		points += n.points;
    		
    		//Ajout à la liste des nutriments de la composante
    		if(component.equals("P"))
    		{
    			p.positifs.add(n);
    		}
    		else
    		{
    			p.negatifs.add(n);
    		}
    		
    	}
    	
    	//Mise à jour des points de la composante dans le produit
    	if(component.equals("P"))
    	{
    		 p.componentP= points;
    	}else
    	{
    		p.componentN= points;
    	}
        
        return p;
    }
    
    
   
}
