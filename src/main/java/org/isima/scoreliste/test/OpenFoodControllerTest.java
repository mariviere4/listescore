package org.isima.scoreliste.test;

import static org.junit.Assert.assertNotNull;

import org.isima.scoreliste.ConfigApi;
import org.isima.scoreliste.Controller.ProduitController;
import org.isima.scoreliste.Entity.Produit;
import org.junit.Test;

public class OpenFoodControllerTest {

	@Test
	public void getProduit() {
		ConfigApi ca = new ConfigApi();
		ProduitController opc = ca.produitController();
		Produit p = opc.getOpenFoodController().getProduit("3029330003533");
		assertNotNull(p);
	}

}
