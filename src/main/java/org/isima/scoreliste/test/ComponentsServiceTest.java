package org.isima.scoreliste.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.isima.scoreliste.ConfigApi;
import org.isima.scoreliste.Controller.ProduitController;
import org.isima.scoreliste.Entity.Nutriment;
import org.isima.scoreliste.Entity.Produit;
import org.isima.scoreliste.Entity.Rule;
import org.junit.Test;

public class ComponentsServiceTest {

	@Test
	public void getRulesByNutriments() {
		ConfigApi ca = new ConfigApi();
		ProduitController pc = ca.produitController();
        ArrayList<Rule> rules = pc.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("rules.json").getFile(), Rule.class);
        ArrayList<String> negatifs = new ArrayList<>();
        negatifs.addAll(Arrays.asList("energy_100g", "saturated-fat_100g", "sugars_100g","salt_100g"));        
		HashMap<String, ArrayList<Rule>> hashRules = pc.getComponentsService().getRulesByNutriment(rules);
		assertNotNull(hashRules);
	}


	@Test
	public void getPointsFromNutriments() {
		ConfigApi ca = new ConfigApi();	
		ProduitController pc = ca.produitController();
		Produit p = pc.getOpenFoodController().getProduit("3029330003533");
        ArrayList<Rule> rules = pc.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("rules.json").getFile(), Rule.class);
        ArrayList<String> negatifs = new ArrayList<>();
        negatifs.addAll(Arrays.asList("energy_100g", "saturated-fat_100g", "sugars_100g","salt_100g"));        
		HashMap<String, ArrayList<Rule>> hashRules = pc.getComponentsService().getRulesByNutriment(rules);
		assertNotNull(hashRules);
		Nutriment n = pc.getComponentsService().getPointsFromNutriment(hashRules, "energy_100g" , p.getValue("energy_100g"), "N");
		assertTrue(n.points == 3);
	}
	
}
