package org.isima.scoreliste.test;

import static org.junit.Assert.assertTrue;

import org.isima.scoreliste.ConfigApi;
import org.isima.scoreliste.Controller.ListeController;
import org.isima.scoreliste.Controller.ProduitController;
import org.isima.scoreliste.services.ListeService;
import org.junit.Test;

public class ListeControllerTest {

	@Test
	public void getProduitsScore() {
		ListeController listec = new ListeController();
		ConfigApi ca = new ConfigApi();
		ProduitController pc = ca.produitController();
		ListeService ls = ca.listeService();
		listec.setProduitController(pc);
		listec.setListeService(ls);
		listec.getProduitsScore();
		listec.listecourses.imprimerListeCourse();
        assertTrue(listec.listecourses != null);
   
	}
}
