package org.isima.scoreliste.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.isima.scoreliste.Controller.FileController;
import org.isima.scoreliste.Entity.Rule;
import org.junit.Test;

public class FileControllerTest {

	@Test
	public void getElementsFromFile() {
		FileController fc = new FileController();
		ArrayList<Rule> rules = fc.getElementsFromFile(getClass().getClassLoader().getResource("rules.json").getFile(), Rule.class);
		assertEquals(56,rules.size());
	}

}
