package org.isima.scoreliste.test;

import static org.junit.Assert.*;

import org.isima.scoreliste.Entity.Produit;
import org.isima.scoreliste.services.OpenFoodService;
import org.junit.Test;

public class OpenFoodServiceTest {

	@Test
	public void getProduit() {
		OpenFoodService opc = new OpenFoodService();
		Produit p = opc.getProduit("3029330003533");
		assertNotNull(p);
	}

}
