package org.isima.scoreliste.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.isima.scoreliste.ConfigApi;
import org.isima.scoreliste.Controller.ProduitController;
import org.isima.scoreliste.Entity.Produit;
import org.isima.scoreliste.Entity.Rule;
import org.isima.scoreliste.Entity.Score;
import org.junit.Test;

public class ProduitControllerTest {

	@Test
	public void calculComponent() {
		ConfigApi ca = new ConfigApi();
		ProduitController pc = ca.produitController();
		Produit p = pc.getOpenFoodController().getProduit("3029330003533");
        ArrayList<Rule> rules = pc.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("rules.json").getFile(), Rule.class);
        ArrayList<String> negatifs = new ArrayList<>();
        negatifs.addAll(Arrays.asList("energy_100g", "saturated-fat_100g", "sugars_100g","salt_100g"));
        HashMap<String, ArrayList<Rule>> hashRules = pc.getComponentsService().getRulesByNutriment(rules);
        p = pc.calculComponent(hashRules, p, rules, negatifs,"N");
        assertTrue(p.componentN == 4);
   
	}
	
	

	@Test
	public void calculScore() {
		ConfigApi ca = new ConfigApi();
		ProduitController pc = ca.produitController();
		Produit p = pc.getOpenFoodController().getProduit("3029330003533");
        ArrayList<Score> scores = pc.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("score.json").getFile(), Score.class);
        ArrayList<Rule> rules = pc.getFileController().getElementsFromFile( getClass().getClassLoader().getResource("rules.json").getFile(), Rule.class);
        ArrayList<String> negatifs = new ArrayList<>();
        negatifs.addAll(Arrays.asList("energy_100g", "saturated-fat_100g", "sugars_100g","salt_100g"));
        ArrayList<String> positifs = new ArrayList<>();
        positifs.addAll(Arrays.asList("fiber_100g", "proteins_100g"));
        HashMap<String, ArrayList<Rule>> hashRules = pc.getComponentsService().getRulesByNutriment(rules);
        p = pc.calculComponent(hashRules, p, rules, negatifs,"N");
        p = pc.calculComponent(hashRules, p, rules, positifs,"P");
        p = pc.calculScore(p, scores);
        assertTrue(p.componentN == 4);
        assertTrue(p.componentP == 10);
        assertTrue(p.score.id == 1);
   
	}

}
