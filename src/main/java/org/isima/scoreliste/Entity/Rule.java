package org.isima.scoreliste.Entity;


/*Règle pour les points à attribuer à chaque élément*/
public class Rule {
    public Long id ;
    public String name ;
    public int points;
    public Double minBound ;
    public String component ;
}
