package org.isima.scoreliste.Entity;

/*Classe intermédiaire pour avoir une liste des nutriments par composante ainsi que les points coreespondants pour le produit*/
public class Nutriment {
	// Nom du nutriment
	public String nutriment;
	// Nom de la composante
	public String component;
	//Nombre de points du nutriment
	public int points;
	
	public Nutriment(String nutriment, String component) {
		super();
		this.nutriment = nutriment;
		this.component = component;
	}
	
	
}
