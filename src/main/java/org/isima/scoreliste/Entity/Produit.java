package org.isima.scoreliste.Entity;

import java.util.ArrayList;


/*Classe récupérée depuis OpenFood puis transformée pour le calcul et renvoyée par notre propre API*/
public class Produit {
	//Nutriments récupérés depuis OpenFoodFacts
    public double energy_100g;
    public  double saturated_fat_100g;
    public double sugars_100g;
    public double salt_100g;
    public double fiber_100g;
    public double proteins_100g;
    
    //Valeurs calculées des composants
    public double componentN;
    public double componentP;
    
    //Listes des nutriments par composants
    public ArrayList<Nutriment> positifs;
    public ArrayList<Nutriment> negatifs;
    
    //Score du produit
    public Score score;
    
    //Code barre du produit
    public Long codeBarre;

    //Constructeur du produit utilisé lors de la récupération des données depuis OpenFoodFacts
    public Produit(double energy_100g, double saturated_fat_100g, double sugars_100g, double salt_100g, double fiber_100g, double proteins_100g, long codeBarre) {
        this.energy_100g = energy_100g;
        this.saturated_fat_100g = saturated_fat_100g;
        this.sugars_100g = sugars_100g;
        this.salt_100g = salt_100g;
        this.fiber_100g = fiber_100g;
        this.proteins_100g = proteins_100g;
        positifs = new ArrayList<>();
        negatifs = new ArrayList<>();
        this.codeBarre = codeBarre;
        
    }

    //Constructeur par défaut
    public Produit()
    {
    	positifs = new ArrayList<>();
        negatifs = new ArrayList<>();
    }
    
    //Récupération de la valeur d'un nutriment 
	public double getValue(String s) {
		double returned = 0;
		if(s.equals("energy_100g"))
			returned = energy_100g;
		else if(s.equals("saturated_fat_100g"))
			returned = saturated_fat_100g;
		else if(s.equals("sugars_100g"))
			returned = sugars_100g;
		else if(s.equals("salt_100g"))
			returned = salt_100g;
		else if(s.equals("fiber_100g"))
			returned = fiber_100g;
		else if(s.equals("proteins_100g"))
			returned = proteins_100g;
		return returned;
	}
	
	//Code barre du produit
	public Long getCodeBarre()
	{
		return codeBarre;
	}
}
