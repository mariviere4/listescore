package org.isima.scoreliste.Entity;

import java.util.ArrayList;
import java.util.List;

import org.isima.scoreliste.ConfigApi;
import org.isima.scoreliste.Controller.ProduitController;

/*Classe liste de course*/
public class Liste {
	
	public List<Produit> listeCourse = new ArrayList<Produit>();
	
	//Ajout du produit p dans la liste
	public void ajouterProduit(Produit p) {
		listeCourse.add(p);
	}
	//Imprimer la liste de course
	public void imprimerListeCourse() {
		listeCourse.stream().forEach(System.out::println);
	}
	
	//Imprimer la totalite de produits dans la liste de course
	public void imprimerTotal() {
		long nbProduit = listeCourse.stream().count();
		System.out.println("Nombre de produits : " + nbProduit);
	}
	
	//Lister des produits par l'odre de code barre
	public void listerParCode() throws Exception {
		ConfigApi ca = new ConfigApi();
		ProduitController pc = ca.produitController();
		Produit p = pc.getOpenFoodController().getProduit("20201456");
		Liste liste = new Liste();
	    liste.ajouterProduit(p);
		//listeCourse.stream().sorted(Comparator.comparingLong(Produit::getCodeBarre)).forEach(System.out::println);
	}
	//code barre : 20201456
	
	

}
