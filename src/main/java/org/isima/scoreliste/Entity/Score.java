package org.isima.scoreliste.Entity;

/*Score pouvant être obtenu par le produit*/
public class Score {
     public int id;
     public String classe;
     public int lowerBound;
     public int upperBound;
     public String color;
}
