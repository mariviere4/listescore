package org.isima.scoreliste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/*
 * Point d'entrée de l'application
 * 
 * 
 */
@SpringBootApplication
public class ScoreListeApp {

    public static void main(String[] args) {
        SpringApplication.run(ScoreListeApp.class, args);
    }
}

