package org.isima.scoreliste.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.isima.scoreliste.Entity.Produit;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
/*Classe de service permettant l'accès à OpenFood*/
public class OpenFoodService {

    /*Récupère les informations du produit dont le code est passé en paramètre*/
    public Produit getProduit(String code) {
        String text = null;
        Produit p = null;
       try{
    	   //Connexion à l'URL d'OpenFoodFacts
           URL url = new URL("https://fr.openfoodfacts.org/api/v0/produit/"+code+".json");
           HttpURLConnection connection = (HttpURLConnection) url.openConnection();
           connection.setDoOutput(true);
           connection.setInstanceFollowRedirects(false);
           connection.setRequestMethod("GET");
           connection.setRequestProperty("Content-Type", "application/json");
           connection.setRequestProperty("charset", "utf-8");
           connection.connect();
           InputStream inStream = connection.getInputStream();
           Scanner scanner = new Scanner(inStream, "UTF-8");
           text = scanner.useDelimiter("\\Z").next();

           System.out.println( text );
           //Transformation en JSON element
           Gson gson = new Gson();
           JsonElement jo =gson.fromJson( text, JsonElement.class );
           //Transformation en objet de type Produit
           p=gson.fromJson( jo.getAsJsonObject().get("product").getAsJsonObject().get("nutriments") , Produit.class);
           scanner.close();
           
       } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p;
    }

}
