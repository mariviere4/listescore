package org.isima.scoreliste.services;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

/*Classe de service permettant l'accès à l'api de liste*/
public class ListeService {
	
    /*Récupère les informations du produit dont le code est passé en paramètre*/
    public List<String> getCodeBarres() {
        String text = null;
        List<String> codebarres= null;
       try{
    	   //Connexion à l'URL de l'api de liste
           URL url = new URL("http://localhost:8082/liste");
           HttpURLConnection connection = (HttpURLConnection) url.openConnection();
           connection.setDoOutput(true);
           connection.setInstanceFollowRedirects(false);
           connection.setRequestMethod("GET");
           connection.setRequestProperty("Content-Type", "application/json");
           connection.setRequestProperty("charset", "utf-8");
           connection.connect();
           InputStream inStream = connection.getInputStream();
           Scanner scanner = new Scanner(inStream, "UTF-8");
           text = scanner.useDelimiter("\\Z").next();

           System.out.println("text" + text );
           //Transformation en JSON element
           Gson gson = new Gson();
           JsonElement jo =gson.fromJson( text, JsonElement.class );
           System.out.println(jo);
           //Transformation en objet de type Produit
           codebarres=gson.fromJson( jo.getAsJsonObject().get("listeCourse") , List.class);
           System.out.println(codebarres);
           scanner.close();
           
       } catch (IOException ex) {
            ex.printStackTrace();
        }
        return codebarres;
    }

}
