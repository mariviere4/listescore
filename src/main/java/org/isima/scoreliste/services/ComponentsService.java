package org.isima.scoreliste.services;

import java.util.ArrayList;
import java.util.HashMap;

import org.isima.scoreliste.Entity.Nutriment;
import org.isima.scoreliste.Entity.Rule;

/*Service de calcule des composantes*/
public class ComponentsService {
	
	/*Calcule les points d'un nutriment pour un produit
	 * rules: map des sous-liste de règles pour chaque nutriment
	 * nutriment: nom du nutriment à analyser
	 * valeurNutriment: valeur du nutriment du produit que l'on évalue
	 * component: Composante à laquelle appartient le nutriment (N ou P)
	 * */
	public Nutriment getPointsFromNutriment(HashMap<String, ArrayList<Rule>> rules,String nutriment, double valeurNutriment,  String component)
	{
		int points = 0;
		Nutriment n = new Nutriment(nutriment, component);
		ArrayList<Rule> rulesNutriment = rules.get(nutriment);
		for(Rule r: rulesNutriment)
		{
			//Si la valeur du nutriment est dans les bornes le nombre de points est attribué au nutriment
			if(r.component.equals(component) && r.minBound < valeurNutriment)
			{
				points = r.points;
				
			}
		}	
		n.points = points;
		return n;
	}
	
	/*Créé la map des sous listes par nutriment*/
	public HashMap<String, ArrayList<Rule>> getRulesByNutriment(ArrayList<Rule> rules)
	{
		//Création de la map des règles par nutriment
		HashMap<String, ArrayList<Rule>> rulesNutriment = new HashMap<>();
		
		//Création des listes de règles pour chaque nutriment
		ArrayList<Rule> energy_100g = new ArrayList<>();
		ArrayList<Rule> saturated_fat_100g = new ArrayList<>();
		ArrayList<Rule> sugars_100g = new ArrayList<>();
		ArrayList<Rule> salt_100g = new ArrayList<>();
		ArrayList<Rule> fiber_100g = new ArrayList<>();
		ArrayList<Rule> proteins_100g = new ArrayList<>();
		
		//Ajout des listes dans la map
		rulesNutriment.put("energy_100g", energy_100g);
		rulesNutriment.put("saturated-fat_100g", saturated_fat_100g);
		rulesNutriment.put("sugars_100g", sugars_100g);
		rulesNutriment.put("salt_100g", salt_100g);
		rulesNutriment.put("fiber_100g", fiber_100g);
		rulesNutriment.put("proteins_100g", proteins_100g);
		
		//Ajout des règles dans les listes correpsondantes
		rules.forEach(r ->
		{
			rulesNutriment.get(r.name).add(r);
		}			
				
		);
		return rulesNutriment;
	}
}
