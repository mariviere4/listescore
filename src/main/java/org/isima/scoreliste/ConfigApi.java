package org.isima.scoreliste;

import org.isima.scoreliste.Controller.FileController;
import org.isima.scoreliste.Controller.OpenFoodController;
import org.isima.scoreliste.Controller.ProduitController;
import org.isima.scoreliste.services.ComponentsService;
import org.isima.scoreliste.services.ListeService;
import org.isima.scoreliste.services.OpenFoodService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



/**
 * @author mariviere4
 * Classe de configuration de l'application
 * 
 */

@Configuration
public class ConfigApi {
	/*URL par défaut d'accès aux données d'OpenFood*/
    @Value( "${openfoodfact.api.url}")
    public String openfood;   

    /*Création du components service*/
    @Bean
    public ComponentsService componentsService()
    {
    	return new ComponentsService();
    }
  
   /*Création du file controller*/ 
    @Bean
    public FileController fileController()
    {
    	return new FileController();
    }
    
    /*Création du openfood controller*/
    @Bean
    public OpenFoodController openFoodController()
    {
    	OpenFoodController opc = new OpenFoodController();
    	OpenFoodService os = openFoodService();
    	opc.setOpenFoodService(os);
    	return opc;
    }
    
    /*Création du opendfood service*/
    @Bean
    public OpenFoodService openFoodService()
    {
    	return new OpenFoodService();
    }
    
    /*Création du produit controllere*/
    @Bean
    public ProduitController produitController()
    {
        ProduitController pc = new ProduitController();
        ComponentsService cs = new ComponentsService();
        FileController fc = new FileController();       
        OpenFoodController opc = openFoodController();
        pc.setComponentsService(cs);
        pc.setFileController(fc);
        pc.setOpenFoodController(opc);
        return pc;
    }
    
    /*Création du liste service*/
    @Bean
	public ListeService listeService() {
		return new ListeService();
	}

    
    
}
