
Une deuxième API est également créée pour récupérer des données sur les produits via l’API publique Open Food Facts. 
Cette API sert à mesurer diverses informations des aliments au niveau nutritif.
Elle doit être lancée après l'API de gestion de liste.

En récupérant les informations sur Open Food Facts, notre application est capable de fournir toutes les informations nutritives d’un produit et son score nutritionnel associé. Le score nutritionnel des aliments repose sur le calcul prenant en compte, pour chaque aliment :

une composante dite « négative » N, qui prend en compte les éléments nutritionnels dont il est recommandé de limiter la consommation : densité énergétique (apport calorique en kJ pour 100 g d’aliment), teneurs en acides gras saturés (AGS), en sucres simples (en g pour 100g d’aliment) et en sel (en mg pour 100g d’aliment);
une composante dite « positive » P, qui prend en compte la teneur de l’aliment en fibres et en protéines.

Puis, la note finale du score nutritionnel attribuée à un produit sera calculé en respectant la formule suivante :
 
Score nutritionnel = Total Points N – Total Points P

Selon la note finale nous pouvons classifier des aliments dans  l’échelle nutritionnelle à cinq niveaux :


| Classe    | Bornes du score | Couleur     |
| --------- | --------------- | ----------- |
| Trop Bon  | Min à -1        | green       |
| Bon       | 0 à 2           | light green |
| Mangeable | 3 à 10          | yellow      |
| Mouai     | 11 à 18         | orange      |
| Degueu    | 19 à Max        | red         |

